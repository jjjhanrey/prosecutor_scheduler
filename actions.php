<!DOCTYPE html>
<html>
<head>
  <title>Actions</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
  <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="vendor/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="vendor/DataTables/Responsive-2.2.1/css/responsive.bootstrap.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="index.php">Prosecutor Scheduler</a>
      </div>
      <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user fa-fw white"></i> <i class="fa fa-caret-down white"></i></a>
          <ul class="dropdown-menu dropdown-user">
            <li>
              <a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li>
            <li>
              <a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul>
        </li>
      </ul>
      <div class="navbar-default sidebar" role="navigation">
        <div class="collapse sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">
            <li>
              <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
              <a href="cases.php"><i class="fa fa-dashboard fa-fw"></i> Cases</a>
            </li>
            <li>
              <a href="reports.php"><i class="fa fa-dashboard fa-fw"></i> Reports</a>
            </li>
            <li>
              <a href="#"><i class="fa fa-gear fa-fw"></i> Settings<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                <li>
                  <a href="complainants.php"><i class="fa fa-dashboard fa-fw"></i> Complainants</a>
                </li>
                <li>
                  <a href="prosecutors.php"><i class="fa fa-dashboard fa-fw"></i> Prosecutors</a>
                </li>
                <li>
                  <a href="procedures.php"><i class="fa fa-dashboard fa-fw"></i> Procedures</a>
                </li>
                <li>
                  <a href="offenses.php"><i class="fa fa-dashboard fa-fw"></i> Offenses</a>
                </li>
                <li>
                  <a href="status.php"><i class="fa fa-dashboard fa-fw"></i> Status</a>
                </li>
                <li>
                  <a href="actions.php"><i class="fa fa-dashboard fa-fw"></i> Actions</a>
                </li>
                <li>
                  <a href="users.php"><i class="fa fa-dashboard fa-fw"></i> Users</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Actions</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Actions
          </div>
          <div class="panel-body">
            <div class="panel-body">
              <?php 
                include ('scripts/connection.php');
                $sql = "SELECT * FROM tbl_actions";
                $result=mysqli_query($conn, $sql);
              ?>
              <div class="table-responsive">
                <table cellspacing="0" class="table table-striped table-bordered" id="tbl_actions" width="100%">
                  <thead>
                    <tr>
                      <th data-priority="1">ID</th>
                      <th data-priority="2">Action</th>
                      <th data-priority="3">Operations</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php while ($row=mysqli_fetch_array($result)) { ?>
                    <tr>
                      <td><?php echo $row['id_action']; ?></td>
                      <td><?php echo $row['action']; ?></td>
                      <td>
                        <button class="btn btn-primary btn-xs editAction" id="<?php echo $row['id_action']; ?>" name='' type="button"><span class="fa fa-edit"></span></button>
                        <button class="btn btn-danger btn-xs deleteAction" id="<?php echo $row['id_action']; ?>" name='' type="button"><span class="fa fa-trash"></span></button></td><?php } ?>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!-- Panel lg-8 end -->
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Add Actions
          </div>
          <div class="panel-body">
            <form action="scripts/add_action.php" id="actionAdd" method="post" name="actionAdd">
              <div class="form-group">
                <label>Action</label> <input class="form-control" name="action" placeholder="" type="text">
              </div>
              <button class="btn btn-primary btn-block" name="addAction" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div><!-- Panel lg-4 end -->
    </div>
    <!-- Modal for editting-->
    <div class="modal fade" id="editAction" role="dialog" tabindex="-1">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Edit Action</h4>
          </div>
          <form action='scripts/edit_action.php' id="actionForm" method='post' name="complainantForm">
            <div class="modal-body" id="actionInfo"></div>
            <div class="modal-footer">
              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
              <button class="btn btn-primary" name="submitEdit" type="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Modal for deleting-->
    <div class="modal fade" id="deleteAction" role="dialog" tabindex="-1">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form action='scripts/delete_action.php' method='post'>
            <div class="modal-header">
              <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Confirm delete</h4>
            </div>
            <div class="modal-body" id="actionInfoDelete"></div>
            <div class="modal-footer">
              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
              <button class="btn btn-danger" name="submitDelete" type="submit">Yes, delete</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="vendor/jquery/jquery.min.js">
  </script> 
  <script src="vendor/bootstrap/js/bootstrap.min.js">
  </script> 
  <script src="vendor/metisMenu/metisMenu.min.js">
  </script> 
  <script src="vendor/dataTables/DataTables-1.10.16/js/jquery.dataTables.js">
  </script> 
  <script src="vendor/dataTables/DataTables-1.10.16/js/dataTables.bootstrap.js">
  </script> 
  <script src="vendor/DataTables/Responsive-2.2.1/js/dataTables.responsive.js">
  </script>
  <script src="js/sidemenu.js">
  </script> 
  <script src="vendor/formValidation/formValidation.js">
  </script> 
  <script src="vendor/formValidation/bootstrap_formValidation.js">
  </script> 
  <script src="vendor/bootstrap-multiselect/bootstrap-multiselect.js">
  </script> 
  <script>
    $(document).ready(function() {

// DataTable
 $('#tbl_actions').dataTable({
  "order": [],
  responsive: true,
  'columnDefs': [
         // { responsivePriority: 1, targets: 0 },
            // { responsivePriority: 2, targets: -1 },
    {
     'searchable': false,
     'targets': [2]
    }, {
      'orderable': false,
     'targets': [2]
    }
   ]
 });
// Ordinary FormValidation
  $('#actionAdd').formValidation({
    excluded: ':disabled',
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      action: {
        validators: {
          notEmpty: {
            message: 'This is a required field.'
          }
        }
      }
    }
  });
// Ajax edit
  $('#tbl_actions').on('click', '.editAction', function() {
    var action_id = $(this).attr('id');
    $.ajax({
      url: "scripts/editAJAX.php",
      method: "post",
      data: {
        type: "action",
        action_id: action_id
      },
      success: function(data) {
        $('#actionInfo').html(data);
        $('#actionForm').formValidation({
          message: 'This value is not valid',
          icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            action: {
              validators: {
                notEmpty: {
                  message: 'This is a required field.'
                }
              }
            }
          }
        });
      }
    });
    $('#editAction').modal("show");
  });
// Ajax delete
  $('#tbl_actions').on('click', '.deleteAction', function() {
    var action_id = $(this).attr('id');
    $.ajax({
      url: "scripts/delete_actionAJAX.php",
      method: "post",
      data: {
        action_id: action_id
      },
      success: function(data) {
        $('#actionInfoDelete').html(data);
      }
    });
    $('#deleteAction').modal("show");
  });
// Bug fix for enter key for the Adding of of complainant
  $('#actionAdd').on("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      $("button[name='addAction']").click();
      return false;
    }
  });
  // Bug fix for enter key for Modal Edit
  $('#actionForm').on("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      $("button[name='submitEdit']").click();
      return false;
    }
  });
  // Destroy Validation when everytime the Modal Closes
  $('#editAction').on('hide.bs.modal', function() {
    $('#actionForm').data('formValidation').destroy();
  });

});
  </script> 
</body>
</html>