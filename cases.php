<?php
$pagename='Cases';
include_once('header.php');
$user = new User();
$cases = new Cases();
?>
    <div class="row">
      <div class="col-lg-12">
      <?php 
        if(Session::exists('case_success')){
          echo '
            <div class="alert alert-success" role="alert">
              '. Session::flash('case_success') .'
            </div>
          ' ;
        } else if(Session::exists('case_error')){
          echo '
            <div class="alert alert-danger" role="alert">
              '. Session::flash('case_error') .'
            </div>
          ' ;
        }
       ?>
        <div class="panel panel-primary">
          <div class="panel-heading">Cases</div>
          <div class="panel-body">
            <div class="panel-body">
              <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Case ID</th>
                    <th>NPS Docket No.</th>
                    <th>Complainant</th>
                    <th>Respondent</th>
                    <th>Offense</th>
                    <th>Prosecutor</th>
                    <th>Resolution</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
              <?php echo $cases->sqlJoin(); ?>
                </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> <!-- Panel lg-8 end -->
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">Add Case</div>
          <div class="panel-body">
            <form class="form-horizontal" id="cases" name="cases" action="scripts/add_case.php" method="POST">
              <div class="form-group">
                <label class="col-sm-2 control-label">NPS Docket No.</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="npsDocket">
                </div>
              </div>
            <div class="form-group">
              <label class="col-md-2 control-label">Complainants</label>
              <div class="col-sm-3">
                <select class="select2 form-control" multiple name="complainant">
                  <?php 
                    $complainant = new Complainants();
                    echo $complainant->listOption();
                  ?>
                </select>
              </div>
            </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Respondent</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="respondent">
                </div>
              </div>
              <div class="form-group">
              <label class="col-md-2 control-label">Offense</label>
              <div class="col-sm-3">
                <select class="form-control" name="offense">
                  <option selected disabled>Select offense</option>
                  <?php 
                    $offense = new Offenses();
                    echo $offense->listOption();
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label">Prosecutor</label>
              <div class="col-sm-3">
                <select class="form-control" name="prosecutor">
                  <option selected disabled>Select prosecutor</option>
                  <?php 
                    $prosecutor = new User();
                    echo $prosecutor->listOption();
                  ?>
                </select>
              </div>
            </div>
            <!--  
            <div class="form-group">
              <label class="col-sm-2 control-label">Action Taken</label>
              <div class="col-sm-3">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tr>
                    <td>2</td>
                    <td>2</td>
                  </tr>         
                </table>
              </div>
              <div class="col-sm-3">
                <select class="form-control">
                  <option selected>1dasdasda</option>
                  <option>2dasdas</option>
                  <option>3dasdas</option>
                  <option>4das</option>
                  <option>5dasdasd</option>
                </select>
              </div>
            </div>
            -->
              <div class="form-group">
                <label class="col-sm-2 control-label">Resolution</label>
                <div class="col-sm-3">
                  <input type="file" class="form-control" name="resolution">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div> <!-- Panel lg-4 end -->
    </div>
  </div>
<?php include 'footer.php'; ?>
  <script>
  $(document).ready(function() {
    $('#example').DataTable({
      responsive: true
    });
        $('select.select2').select2({
      placeholder: 'Select a complainant.'
    });
  });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {

        $('#csases').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 npsDocket: {
                    validators: {
                        notEmpty: {
                            message: 'This is a required field.'
                        },
                        regexp: {
                            regexp: /^([0-9])|^([0-9.]+\d{2})/,
                            message: 'Invalid character detected.'
                        }
                    }
                },
                complainant: {
                    validators: {
                        notEmpty: {
                            message: 'This is a required field.'
                        }
                    }
                },
                respondent: {
                    validators: {
                        notEmpty: {
                            message: 'This is a required field.'
                        }
                    }
                },
                offense: {
                  validators: {
                    notEmpty: {
                      message: 'This is a required field.'
                    }
                  }
                },
                prosecutor: {
                  validators: {
                    notEmpty: {
                      message: 'This is a required field.'
                    }
                  }
                },
                resolution: {
                validators: {
                  notEmpty: {
                      message: 'This is a required field.'
                    },
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 10*1024*1024,
                        message: 'Please choose a pdf file with a size less than 10M.'
                    }
                }
            }         
            }
        });
    });
</script>
</body>

</html>