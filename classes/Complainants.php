<?php

class Complainants {
	
	private $_db,
			$_data,
			$_table = 'tbl_complainants';
			
	public function __construct($id = null){
		$this->_db = DB::getInstance();
		if($id){
			$this->find($id);
		}
	}
	
	public function create($fields = array()){
		if($this->_db->insert($this->_table, $fields)){
			throw new Exception('There was a problem creating a complainant.');
		}
	}
	
	public function update($fields = array(), $id = null){
		
		if(!$this->_db->update($this->_table, $id, $fields, 'id_complainant')){
			throw new Exception('There was a problem updating.');
		}
	}
	
	public function delete($fields = array()){
		
		$id_count = count($fields); // count array()
		
		for($i=0; $i < $id_count; $i++) {
		
	        $id = $fields[$i];
	        
	        $column = array('id_complainant','=',$id);
	        
	        if(!$this->_db->delete($this->_table, $column)){
				throw new Exception('There was a problem deleting.');
			}
	        
	    }
		
	}
	
	public function find($id = null){
		
		if($id){
			$field = (is_numeric($id)) ? 'id_complainant' : 'complainant';
			$data = $this->_db->get($this->_table, array($field, '=', $id));
		}
		
		if($data->count()){
			$this->_data = $data->first();
			return true;
		}
		
		return  false;
	}
	
	public function getList(){
		$posts = $this->_db->queryAll("SELECT * FROM {$this->_table}");
		if($posts->count()){
			$results = '';
			foreach($posts->results() as $post){
				$results .= '
				<tr>
				<td>'.$post[2].', '.$post[1]. '</td>
				<td>
					<button class="btn btn-primary btn-xs edit" id="'.$post[0].'" name="" type="button"><span class="fa fa-edit"></span></button>
          <button class="btn btn-danger btn-xs delete" id="'.$post[0].'" name="" type="button"><span class="fa fa-trash"></span></button>
				</td>
				</tr>';
			}
			
			return $results;
		}
		//return '<tr><td colspan="2">No Result Found</td></tr>';
	}

	public function listOption(){
		$posts = $this->_db->queryAll("SELECT * FROM {$this->_table}");
		if($posts->count()){
			$results = '';
			foreach($posts->results() as $post){
				$results .= '
				<option value="'.$post[0].'">'.$post[1].' '.$post[3].' '.$post[2].'</option>
				<tr>
				<td>'.$post[2].', '.$post[1]. '</td>
				<td>
					<button class="btn btn-primary btn-xs edit" id="'.$post[0].'" name="" type="button"><span class="fa fa-edit"></span></button>
          <button class="btn btn-danger btn-xs delete" id="'.$post[0].'" name="" type="button"><span class="fa fa-trash"></span></button>
				</td>
				</tr>';
			}
			
			return $results;
		}
		//return '<tr><td colspan="2">No Result Found</td></tr>';
	}
	
	public function option($table, $name, $selected='', $where='', $orderby, $requiredfield=false, $enable=false){
		
		if(isset($orderby)){
			$order = 'ORDER BY '.$orderby.' ASC';
		}else{
			$order = 'ORDER BY '.$name.' ASC';
		}
		$required = ($requiredfield) ? ' required' : '';
		
		if(isset($where) && $where=='1'){
			$whereis = "WHERE field_id=1 && field2_id=3";
		}else{
			$whereis = '';
		}
		
		$options = $this->_db->queryAll("SELECT * FROM {$table} {$whereis} {$order}");
		
		$results = "<select class='form-control' id='{$name}' {$required} name='{$name}'>";
		
		if($options->count()){
			$results .= '<option></option>';
			foreach($options->results() as $option){
				$results .= '<option '.($selected == $option[0] ? 'selected' : '').'  class="'.($enable ? $option[2] : 'false').'" value="'.$option[0].'">'.$option[1].'</option>';
			}
			
		}else{
			$results .= "<option>No Data</option>";
		}
		
		$results .= "</select>";
		
		return $results;
			
	}
	
	public function data(){
		return $this->_data;
	}
}