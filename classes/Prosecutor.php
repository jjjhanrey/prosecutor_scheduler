<?php

class Prosecutor {
	
	private $_db,
			$_data,
			$_table = 'tbl_users';
			
	public function __construct($id = null){
		$this->_db = DB::getInstance();
		if($id){
			$this->find($id);
		}
	}
	
	public function create($fields = array()){
		
		if($this->_db->insert($this->_table, $fields)){
			throw new Exception('There was a problem creating a name.');
		}
		
	}
	
	public function update($fields = array(), $id = null){
		
		if(!$this->_db->update($this->_table, $id, $fields, 'id_user')){
			throw new Exception('There was a problem updating.');
		}
	}
	
	public function delete($fields = array()){
		$id_count = count($fields); // count array()
		for($i=0; $i < $id_count; $i++) {
	        $id = $fields[$i];
	        $column = array('id_user','=',$id);
	        if(!$this->_db->delete($this->_table, $column)){
				throw new Exception('There was a problem deleting.');
			}
	  }
	}
	
	public function find($id = null){
		
		if($id){
			// $field = (is_numeric($id)) ? 'id_prosecutor' : 'firstname';
			$field = (is_numeric($id)) ? 'id_user' : 'id_user';

			$data = $this->_db->get($this->_table, array($field, '=', $id));
		}
		
		if($data->count()){
			$this->_data = $data->first();
			return true;
		}
		
		return  false;
	}
	
	public function getList(){
		
		$posts = $this->_db->queryAll("SELECT * FROM {$this->_table}");
		if($posts->count()){
			$results = '';
			foreach($posts->results() as $post){
				$results .= '
				<tr>
				<td>'.$post[3].' '.$post[2].' '.$post[4].'</td>
				<td>
					<button class="btn btn-primary btn-xs edit" id="'.$post[0].'" name="" type="button"><span class="fa fa-edit"></span></button>
          <button class="btn btn-danger btn-xs delete" id="'.$post[0].'" name="" type="button"><span class="fa fa-trash"></span></button>
				</td>
				</tr>';
			}
			
			return $results;
		}
		//return '<tr><td colspan="2">No Result Found</td></tr>';
	}
	
	public function option($table, $name, $selected='', $where='', $orderby, $requiredfield=false, $enable=false){
		
		if(isset($orderby)){
			$order = 'ORDER BY '.$orderby.' ASC';
		}else{
			$order = 'ORDER BY '.$name.' ASC';
		}
		$required = ($requiredfield) ? ' required' : '';
		
		if(isset($where) && $where=='1'){
			$whereis = "WHERE field_id=1 && field2_id=3";
		}else{
			$whereis = '';
		}
		
		$options = $this->_db->queryAll("SELECT * FROM {$table} {$whereis} {$order}");
		
		$results = "<select class='form-control' id='{$name}' {$required} name='{$name}'>";
		
		if($options->count()){
			$results .= '<option></option>';
			foreach($options->results() as $option){
				$results .= '<option '.($selected == $option[0] ? 'selected' : '').'  class="'.($enable ? $option[2] : 'false').'" value="'.$option[0].'">'.$option[1].'</option>';
			}
			
		}else{
			$results .= "<option>No Data</option>";
		}
		
		$results .= "</select>";
		
		return $results;
			
	}

		public function login($username = null,$password = null, $remember = false){
		$user = $this->find($username);
		if(!$username && !$password && $this->exists()){
			// log user in 
			Session::put($this->_sessionName,$this->data()->id);
		} else {

		//print_r($this->_data);
			if ($user){
				// $this->isLoggedIn()->id;
				if($this->data()->password === Hash::make($password, $this->data()->salt)){
					// This line fails!
					Session::put($this->_sessionName,$this->data()->id);

						if($remember){
							echo 'Remember';
							$hash = Hash::unique();
							$hashCheck = $this->_db->get('users_session', array('id_user', '=', $this->data()->id));
							if(!$hashCheck->count()){
									echo 'No count';
									echo $this->data()->id; //exit;
								$this->_db->insert('users_session', array(
									'id_user' => $this->data()->id,
									'hash' => $hash
									));
							}else{
								$hash = $hashCheck->first()->hash;
							}
							Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
						}
						return true;
					//echo 'OK!';
				}
				
			}
		return false;
		}

	}
	
	public function data(){
		return $this->_data;
	}
}