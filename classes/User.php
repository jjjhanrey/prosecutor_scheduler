<?php
class User {
	public $_db,
					$_data,
					$_sessionName,
					$_cookieName,
					$_isLoggedIn;

	public function __construct($user = null) {
		$this->_db = DB::getInstance();
		
		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName = Config::get('remember/cookie_name');

		if(!$user){
			if(Session::exists($this->_sessionName)){
				$user = Session::get($this->_sessionName);
				if($this->find($user)){
					$this->_isLoggedIn = true;
				} else {
					// process logout
				}

			}
		} else {
			$this->find($user);
		}
	}


	public function update($fields = array(),$id=null){

		if(!$id && $this->isLoggedIn()){
			$id=$this->data()->id_user; 
		}

		if(!$this->_db->update('tbl_users', $id, $fields, 'id_user')){
			throw new Exception('There was a problem updating.');
		}
	}

	public function create($fields=array()){
		if (!$this->_db->insert('users', $fields)){
			throw new Exception('There was a problem creating the account.');
		}
	}

	public function find($user = null){
		if($user){
			$field = (is_numeric($user)) ? 'id_user' : 'username';
			$data = $this ->_db->get('tbl_users',array($field, "=", $user));

			if($data->count()){
				$this->_data = $data->first();
				return true;
			}
		}

		return false;

	}


		public function getList(){
		$posts = $this->_db->queryAll("SELECT * FROM tbl_users");
		if($posts->count()){
			$results = '';
			foreach($posts->results() as $post){
				$results .= '
				<tr>
				<td>'.$post[6].'</td>
				<td>'.$post[3].' '.$post[2].' '.$post[4].'</td>
				<td>
					<button class="btn btn-primary btn-xs edit" id="'.$post[0].'" name="" type="button"><span class="fa fa-edit"></span></button>
          <button class="btn btn-danger btn-xs delete" id="'.$post[0].'" name="" type="button"><span class="fa fa-trash"></span></button>
				</td>
				</tr>';
			}
			
			return $results;
		}
		//return '<tr><td colspan="2">No Result Found</td></tr>';
	}

public function listOption(){
		$posts = $this->_db->queryAll("SELECT * FROM tbl_users");
		if($posts->count()){
			$results = '';
			foreach($posts->results() as $post){
				$results .= '
				<option value="'.$post[0].'">'.$post[3].' '.$post[2].' '.$post[4].'</option>
				';
			}
			return $results;
		}
	}

	public function option($table, $name, $selected='', $where='', $orderby, $requiredfield=false, $enable=false){
		
		if(isset($orderby)){
			$order = 'ORDER BY '.$orderby.' ASC';
		}else{
			$order = 'ORDER BY '.$name.' ASC';
		}
		$required = ($requiredfield) ? ' required' : '';
		
		if(isset($where) && $where=='1'){
			$whereis = "WHERE field_id=1 && field2_id=3";
		}else{
			$whereis = '';
		}
		
		$options = $this->_db->queryAll("SELECT * FROM {$table} {$whereis} {$order}");
		
		$results = "<select class='form-control' id='{$name}' {$required} name='{$name}'>";
		
		if($options->count()){
			$results .= '<option></option>';
			foreach($options->results() as $option){
				$results .= '<option '.($selected == $option[0] ? 'selected' : '').'  class="'.($enable ? $option[2] : 'false').'" value="'.$option[0].'">'.$option[1].'</option>';
			}
			
		}else{
			$results .= "<option>No Data</option>";
		}
		
		$results .= "</select>";
		
		return $results;
			
	}

	public function hasPermission($key) {
		// $group = $this->_db->get('tbl_group',array('id','=',$this->data()->id));
		$group = $this->_db->get('tbl_group',array('id','=',$this->data()->permission));
		
		print_r($group->first());

		if ($group->count()){
		  $permissions = json_decode($group->first()->permission,true);

			print_r($permissions);

		  if($permissions[$key] == true){
		  	return true;

		  }

		}

		return false;
	}

	public function login($username = null,$password = null, $remember = false){
		$user = $this->find($username);
		if(!$username && !$password && $this->exists()){
			// log user in 
			Session::put($this->_sessionName,$this->data()->id_user);
		} else {

		//print_r($this->_data);
			if ($user){
				// $this->isLoggedIn()->id;
				if($this->data()->password === Hash::make($password, $this->data()->salt)){
					// This line fails!
					Session::put($this->_sessionName,$this->data()->id_user);

						if($remember){
							echo 'Remember';
							$hash = Hash::unique();
							$hashCheck = $this->_db->get('tbl_user_session', array('id_user', '=', $this->data()->id_user));
							if(!$hashCheck->count()){

									echo 'No count';
									echo $this->data()->id; //exit;
								$q= $this->_db->insert('tbl_user_session', array(
									'id_user' => $this->data()->id_user,
									'hash' => $hash
									));
								if ($q) {
								}
							}else{
								$hash = $hashCheck->first()->hash;
							}
							Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
						}
						return true;
					echo 'OK!';
				}
				
			}
		return false;
		}

	}

	public function exists(){
		return (!empty($this->_data)) ? true : false;
	}

	public function logout(){

		$this->_db->delete('users_session',array('user_id','=',$this->data()->user_id));
		Session::delete($this->_sessionName);
		Cookie::delete($this->_cookieName);
	} 

	public function data(){
		return $this->_data;
	}

	public function isLoggedIn(){
		return $this->_isLoggedIn;
	} 
}