<?php
$pagename='Complainants';
include_once('header.php');
$setting = new Complainants();
?>
    <div class="row">
      <div class="col-lg-8">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Complainants
          </div>
          <div class="panel-body">
            <div class="panel-body">
              <div class="table-responsive">
                <table cellspacing="0" class="table table-striped table-bordered" id="tbl_complainants" width="100%">
                  <thead>
                    <tr>
                      <th data-priority="1">Name</th>
                      <th data-priority="2">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
              <?php  echo $setting->getList();?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!-- Panel lg-8 end -->
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Add Complainant
          </div>
          <div class="panel-body">
            <form action="scripts/add_complainant.php" id="complainantAdd" method="post" name="complainantAdd">
              <div class="form-group">
                <label>Last Name</label> <input class="form-control" name="lastname" placeholder="" type="text">
              </div>
              <div class="form-group">
                <label>First Name</label> <input class="form-control" name="firstname" placeholder="" type="text">
              </div>
              <div class="form-group">
                <label>Middle Name</label> <input class="form-control" name="middlename" placeholder="" type="text">
              </div>
              <!-- <div class="form-group">
                <label>Gender</label> <select class="form-control" name="gender">
                  <option disabled selected>
                    -Select Gender-
                  </option>
                  <option value="0">
                    Male
                  </option>
                  <option value="1">
                    Female
                  </option>
                </select>
              </div> -->
              <button class="btn btn-primary btn-block" name="addComplainant" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div><!-- Panel lg-4 end -->
    </div>
    
  </div>
  <?php include 'modal.php' ?>

  <?php include 'footer.php' ?>
  <script>
    $(document).ready(function(){
// DataTable
 $('#tbl_complainants').dataTable({
  responsive: true,
  "order": [],
  'columnDefs': [
         // { responsivePriority: 1, targets: 0 },
            // { responsivePriority: 2, targets: -1 },
    {
     'searchable': false,
     'targets': [1]
    }, {
      'orderable': false,
     'targets': [1]
    }
   ]
 });


$('.delete').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'delete',
          page: 'complainant'
        },
        success: function(data) {
          $('#infoDelete').html(data);
        }
      });
      $('#deleteModal').modal("show");
    });
    $('.edit').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'edit',
          page: 'complainant'
        },
        success: function(data) {
          $('#infoEdit').html(data);
        }
      });
      $('#editModal').modal("show");
    });


// Ordinary FormValidation
  $('#complainantAdd').formValidation({
    excluded: ':disabled',
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      lastname: {
        validators: {
          notEmpty: {
            message: 'This is a required field.'
          }
        }
      },
      firstname: {
        validators: {
          notEmpty: {
            message: 'This is a required field.'
          }
        }
      },
      middlename: {
        validators: {
          notEmpty: {
            message: 'This is a required field.'
          }
        }
      },
      gender: {
        validators: {
          notEmpty: {
            message: 'This is a required field.'
          }
        }
      },
      browsers: {
        validators: {
          callback: {
            message: 'Please choose 2-3 browsers you use for developing',
            callback: function(value, validator, $field) {
              // Get the selected options
              var options = validator.getFieldElements('browsers').val();
              return (options != null && options.length >= 2 && options.length <= 3);
            }
          }
        }
      }
    }
  }).find('[name="browsers"]').multiselect({
    buttonWidth: '100px',
    maxHeight: 400,
    enableFiltering: true,
    includeSelectAllOption: true,
    // Re-validate the multiselect field when it is changed
    onChange: function(element, checked) {
      $('#multiselectForm').formValidation('revalidateField', 'browsers');
    }
  }).end();
// Ajax edit
  // $('#tbl_complainants').on('click', '.editComplainant', function() {
  //   var complainant_id = $(this).attr('id');
  //   $.ajax({
  //     url: "scripts/editAJAX.php",
  //     method: "post",
  //     data: {
  //       type: "complainant",
  //       complainant_id: complainant_id
  //     },
  //     success: function(data) {
  //       $('#complainantInfo').html(data);
  //       $('#complainantForm').formValidation({
  //         message: 'This value is not valid',
  //         icon: {
  //           valid: 'glyphicon glyphicon-ok',
  //           invalid: 'glyphicon glyphicon-remove',
  //           validating: 'glyphicon glyphicon-refresh'
  //         },
  //         fields: {
  //           lastname: {
  //             validators: {
  //               notEmpty: {
  //                 message: 'This is a required field.'
  //               }
  //             }
  //           },
  //           firstname: {
  //             validators: {
  //               notEmpty: {
  //                 message: 'This is a required field.'
  //               }
  //             }
  //           },
  //           middlename: {
  //             validators: {
  //               notEmpty: {
  //                 message: 'This is a required field.'
  //               }
  //             }
  //           },
  //           gender: {
  //             validators: {
  //               notEmpty: {
  //                 message: 'This is a required field.'
  //               }
  //             }
  //           }
  //         }
  //       });
  //     }
  //   });
  //   $('#editComplainant').modal("show");
  // });
// Ajax delete
  // $('#tbl_complainants').on('click', '.deleteComplainant', function() {
  //   var complainant_id = $(this).attr('id');
  //   $.ajax({
  //     url: "scripts/delete_complainantAJAX.php",
  //     method: "post",
  //     data: {
  //       complainant_id: complainant_id
  //     },
  //     success: function(data) {
  //       $('#complainantInfoDelete').html(data);
  //     }
  //   });
  //   $('#deleteComplainant').modal("show");
  // });
// Bug fix for enter key for the Adding of of complainant
  $('#complainantAdd').on("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      $("button[name='submitAddComplainant']").click();
      return false;
    }
  });
  // Bug fix for enter key for Modal Edit
  $('#complainantForm').on("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      $("button[name='theSubmit']").click();
      return false;
    }
  });
  // Destroy Validation when everytime the Modal Closes
  $('#editComplainant').on('hide.bs.modal', function() {
    $('#complainantForm').data('formValidation').destroy();
  });

});
  </script>