<?php
/*
 * 
 * Filename: init.php
 * 
 */

session_start();
ob_start();

$GLOBALS['config'] = array(
	'mysql'	=> array(
		'host'		=> 'localhost', //127.0.0.1
		'username'	=> 'root', // 
		'password'	=> '', // 
		'db'		=> 'db_prosecutorapp' // 
	),
			'remember' => array(
					'cookie_name' => 'hash',
					'cookie_expiry' => 604800
			),
			'session' => array(
					'session_name' => 'user',
					'token_name' => 'token'
			)
);
/* Auto load all classes */
spl_autoload_register(function ($class){
	require_once('classes/'. $class .'.php');
});

// Add your other config here:
require_once('functions/sanitize.php');

if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))){
	// echo 'User asked to be remembered';
	$hash = Cookie::get(Config::get('remember/cookie_name'));
	$hashCheck = DB::getInstance()->get('tbl_user_session',array('hash','=',$hash));

	if($hashCheck->count()){
		echo 'Hash matches. Log user in.';
		//echo "Check";
		$user = new User($hashCheck->first()->id_user);
		$user ->login();
	}

}