<?php
/*
 * 
 * Filename: init.php
 * 
 */

session_start();
ob_start();

$GLOBALS['config'] = array(
	'mysql'	=> array(
		'host'		=> 'localhost', //127.0.0.1
		'username'	=> 'root', // 
		'password'	=> '', // 
		'db'		=> 'db_prosecutorapp' // 
	),
			'remember' => array(
					'cookie_name' => 'hash',
					'cookie_expiry' => 604800
			),
			'session' => array(
					'session_name' => 'user',
					'token_name' => 'token'
			)
);
/* Auto load all classes */
spl_autoload_register(function ($class){
	require_once('../classes/'. $class .'.php');
});

// Add your other config here:
require_once('../functions/sanitize.php');