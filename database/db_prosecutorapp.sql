-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2018 at 04:48 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_prosecutorapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_actions`
--

CREATE TABLE `tbl_actions` (
  `id_action` int(11) NOT NULL,
  `action` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_actions`
--

INSERT INTO `tbl_actions` (`id_action`, `action`) VALUES
(1, 'File in Court'),
(2, 'Dismissed'),
(3, 'Motion Reconsideration'),
(5, 'Editted');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_actiontaken`
--

CREATE TABLE `tbl_actiontaken` (
  `tactiontaken_id` int(10) NOT NULL,
  `case_id` int(10) NOT NULL,
  `date` datetime(6) NOT NULL,
  `procedure_id` int(10) NOT NULL,
  `status_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cases`
--

CREATE TABLE `tbl_cases` (
  `cases_id` int(10) NOT NULL,
  `nps_docket_no` varchar(50) NOT NULL,
  `complainant_id` longtext NOT NULL,
  `respondent` varchar(50) NOT NULL,
  `offense_id` int(10) NOT NULL,
  `prosecutor_id` int(10) NOT NULL,
  `resolution_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cases`
--

INSERT INTO `tbl_cases` (`cases_id`, `nps_docket_no`, `complainant_id`, `respondent`, `offense_id`, `prosecutor_id`, `resolution_id`) VALUES
(0, '1', '1', '1', 1, 1, 1),
(1, '123', '123', '123', 123, 134, 123);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complainants`
--

CREATE TABLE `tbl_complainants` (
  `id_complainant` int(20) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_complainants`
--

INSERT INTO `tbl_complainants` (`id_complainant`, `lastname`, `firstname`, `middlename`, `gender`) VALUES
(1, 'Doe', 'John\'', 'Smith', 0),
(2, 'Gonzales', 'Jhan Rey', 'Lardizabal', 0),
(4, 'Jimenez', 'Junife', 'Reganit', 1),
(5, 'Lazaro', 'Teresa', 'Ewan', 1),
(9, 'Padre', 'Ralph John', 'Jose', 0),
(10, 'Manarang', 'Emmanuel glenn', 'Pogi', 0),
(12, 'a', 'a', 'a', 0),
(13, 'h', 'h', 'h', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group`
--

CREATE TABLE `tbl_group` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `permission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_group`
--

INSERT INTO `tbl_group` (`id`, `name`, `permission`) VALUES
(1, 'Standard User', ''),
(2, 'Administrator', '{\"admin\" :1}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offenses`
--

CREATE TABLE `tbl_offenses` (
  `id_offense` int(11) NOT NULL,
  `offense` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_offenses`
--

INSERT INTO `tbl_offenses` (`id_offense`, `offense`) VALUES
(3, 'Offense 3'),
(4, 'Offense 4'),
(5, 'Offense 5'),
(6, 'Offense 1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_procedures`
--

CREATE TABLE `tbl_procedures` (
  `id_procedure` int(11) NOT NULL,
  `proceduretaken` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_procedures`
--

INSERT INTO `tbl_procedures` (`id_procedure`, `proceduretaken`) VALUES
(1, 'Procedure 1'),
(7, 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prosecutors`
--

CREATE TABLE `tbl_prosecutors` (
  `id_prosecutor` int(20) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `gender` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_prosecutors`
--

INSERT INTO `tbl_prosecutors` (`id_prosecutor`, `lastname`, `firstname`, `middlename`, `gender`) VALUES
(1, 'Doe', 'John', 'Smith', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resolutions`
--

CREATE TABLE `tbl_resolutions` (
  `resolution_id` int(10) NOT NULL,
  `resolution` int(100) NOT NULL,
  `date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id_status` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id_status`, `status`) VALUES
(1, 'File in Court'),
(3, 'Motion Reconsideration'),
(4, 'Resolved for Filling'),
(6, 'da');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id_user` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `gender` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id_user`, `permission`, `lastname`, `firstname`, `middlename`, `gender`, `username`, `password`, `salt`) VALUES
(5, 0, 'Jhan REy', 'FSAFA', 'FSAFS', 0, '123', '123', ''),
(15, 0, 'a', 'a', 'a', 0, 'as', 'sa', ''),
(16, 0, 'asasdasdasdasdasdas', 'as', 'sa', 0, 'asa', 'asa', ''),
(20, 2, 'Gonzales', 'Jhan Rey', 'Lardizabal', 0, 'jjjhanrey', '7d7eace6a4babe18bd79742ecd7e2df60eccd778450227860e9ec5b7dd6f4d58', 'nŠ´nW(¶—†›•C©Žã,÷K¬h#ÛƒÛh,\\¾1—Š'),
(21, 2, '1', '1', '1', 0, '123456789', 'c6b3ba44bfee7c8ffd0c222977caf1b2894273ed78bd21a98a116eb4369b17ab', 'òtégo¥Q§™p”ÈàðÿúšhÕMŒê‚h&äž'),
(22, 2, '123', '123', '123', 0, '112312312', '8860333cba3b9b0cbb1936111809a082b09561cdacee7335be74c48f54d78ece', '$Ö7´wJÑ°rP‰LËõ=†³»ù7¿.ÎEöùÇ'),
(23, 2, '123', '123', '123', 0, '11231231222', 'bbc5dc477e5d485f877e6d0748eb36c618a0b25f3f1e6282d2050f431695bc67', '[UkÚ¯1îå*(TEð„õœ‡ó9 Zs{þ'),
(24, 2, '123', '123', '123', 0, '123456', '0f8251993150bd99d5530e4935fe2993d5a69c2e01803a5435ce96ddc6904f5f', '2¬05æéx?\\ÃÃ±\Z_vwÉ\nEë\nmÂ÷ó ¦C7'),
(25, 1, '1234', '1234', '1234', 0, '123123ssa1', '22bcb261bae8e0ec3c264bce20843f99ea4011776416d39ae0a359ff60fa32ad', 'Ü´î÷—éY^f¼sE±oZ‘«¤<›¦ºâÄŒMq'),
(26, 5, '1', '1', '1', 0, '12345612', '7df0695e893fa943bc488be572bb00382a49162f7d85bd3bf1d2ca890e51fd42', 'tã´—µ‘LktìY&PNÙ¹#-R ?ÚÉ&‚?ò'),
(27, 2, 'Gonzales', 'Reymart', 'Lardizabal', 0, 'emat', '41846c8b930eb6abb626e8492e44cf5ea283d1c4a93e6dbb3c84d0744e070d6b', 'ÿýõÀÛSn£H¾NÆßÁa\\½Ö(o™už¥©«Á'),
(28, 2, 'Gonzales', 'Reymart', 'Lardizabal', 0, 'emat07', 'addfd2098f9800e887503909eda59a782f07fcc77ed9416a80bd963d28016c48', 'ETÞ]÷±*ž»p˜lãa^:g[¸ l1ƒ\\Ä8yk'),
(29, 2, '1', '1', '1', 0, 'ematt', '442300026e86c88c416560f14ea16f845c5966edabce0555fe82f9f5be9e4d76', '‘©rÓŒ÷4í”4t]/ÞIóÙæ{¨5Nr6Aé'),
(30, 2, '1', '1', '1', 0, 'ematttt', 'a12962b20e1a86a757a5072d6ccea9498f1542eee0f8692f3f368ad16e0c6a01', 'ötføNö+ÚÆÖ`\\h½(2o@\'&Àx£OúG¥À'),
(31, 2, '1', '1', '1', 0, 'Emattttttttttt', 'f2beff8060bdae42052bf31c4d8ac263cc99f8528e5e112079d538111ea9f5b3', '\"åùtÙBØvÈee%d`ÂäÃ1ÈIÍ7ò¶SÎ'),
(35, 2, 'all', 'all', 'all', 0, 'all1', '81d1875faca211780a78aef02361ff27782b81b18fed6b2fe3c94628f9470848', 'õ‹õ¨}÷à„œZµºÔáLúÉÑ.°:AQ}Ö'),
(36, 1, 'Last', 'First', 'Middle', 0, 'Newuser', 'ad955e1fb32fa572c777c42fec4a5f37e3eb1458c81b3c72270f02b0ed377324', 'ƒÈŒ.Â“°¯\')ø¬iÇd6ä©uŽX`ÆÅN*FâY');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_session`
--

CREATE TABLE `tbl_user_session` (
  `id` int(11) NOT NULL,
  `id_user` int(100) NOT NULL,
  `hash` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_session`
--

INSERT INTO `tbl_user_session` (`id`, `id_user`, `hash`) VALUES
(1, 27, 'b6bb3b28f2f985208db6ed61818bf540492d8a63738d2bca368b1f2f0281cc47'),
(2, 25, '42267c62d4ed2d4ac7645a9680f4aa5a22ce527cbf35a61fc78242e15cdddad5'),
(3, 30, '21ec1f3cb9d993d6ba26437cb2b37512890516ce001eff4422fa83b36646ef15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_actions`
--
ALTER TABLE `tbl_actions`
  ADD PRIMARY KEY (`id_action`);

--
-- Indexes for table `tbl_complainants`
--
ALTER TABLE `tbl_complainants`
  ADD PRIMARY KEY (`id_complainant`);

--
-- Indexes for table `tbl_group`
--
ALTER TABLE `tbl_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_offenses`
--
ALTER TABLE `tbl_offenses`
  ADD PRIMARY KEY (`id_offense`);

--
-- Indexes for table `tbl_procedures`
--
ALTER TABLE `tbl_procedures`
  ADD PRIMARY KEY (`id_procedure`);

--
-- Indexes for table `tbl_prosecutors`
--
ALTER TABLE `tbl_prosecutors`
  ADD PRIMARY KEY (`id_prosecutor`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tbl_user_session`
--
ALTER TABLE `tbl_user_session`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_actions`
--
ALTER TABLE `tbl_actions`
  MODIFY `id_action` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_complainants`
--
ALTER TABLE `tbl_complainants`
  MODIFY `id_complainant` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_group`
--
ALTER TABLE `tbl_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_offenses`
--
ALTER TABLE `tbl_offenses`
  MODIFY `id_offense` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_procedures`
--
ALTER TABLE `tbl_procedures`
  MODIFY `id_procedure` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_prosecutors`
--
ALTER TABLE `tbl_prosecutors`
  MODIFY `id_prosecutor` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_user_session`
--
ALTER TABLE `tbl_user_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
