  <script src="assets/vendors/jquery/jquery.min.js"></script> 
  <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script> 
  <script src="assets/vendors/metisMenu/metisMenu.min.js"></script> 
  <script src="assets/vendors/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script> 
  <script src="assets/vendors/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js"></script>
  <script src="assets/vendors/DataTables/Responsive-2.2.1/js/dataTables.responsive.js"></script>
  <script src="assets/js/sidemenu.js"></script> 
  <script src="assets/vendors/formValidation/formValidation.js"></script> 
  <script src="assets/vendors/formValidation/bootstrap_formValidation.js"></script> 
  <script src="assets/vendors/bootstrap-multiselect/bootstrap-multiselect.js"></script> 
  <script src="assets/vendors/select2/select2.js"></script>
  <script src="assets/vendors/bootstrap-datetimepicker/js/moment.js"></script>
  <script src="assets/vendors/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>