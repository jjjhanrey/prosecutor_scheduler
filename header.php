<!DOCTYPE html>
<html>
<head>
  <title><?php echo $pagename; ?></title>
  <?php 
    require_once('core/init.php'); 
    $user = new User();
    if (!$user->isLoggedIn()) {
      Session::flash('login','Please login to browse.');
      Redirect::to('login.php');
    }
  ?>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
  <link href="assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/DataTables/Responsive-2.2.1/css/responsive.bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/metisMenu/metisMenu.min.css" rel="stylesheet">
  <link href="assets/vendors/select2/select2.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendors/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">


</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="index.php">Prosecutor Scheduler</a>
      </div>
      <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user fa-fw white"></i> <i class="fa fa-caret-down white"></i></a>
          <ul class="dropdown-menu dropdown-user">
            <!-- <li>
              <a href="profile.php"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li> -->
            <li>
              <a href="myaccount.php"><i class="fa fa-gear fa-fw"></i> My Account 
              <!-- <?php 
                $user = new User();
                echo '('.$user->data()->username.')';
               ?> -->
              </a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="scripts/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul>
        </li>
      </ul>
      <div class="navbar-default sidebar" role="navigation">
        <div class="collapse sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">
            <li>
              <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
              <a href="cases.php"><i class="fa fa-briefcase fa-fw"></i> Cases</a>
            </li>
            <li>
              <a href="complainants.php"><i class="fa fa-users fa-fw"></i> Complainants</a>
            </li>
            <!-- <li>
              <a href="#"><i class="fa fa-book fa-fw"></i> Reports</a>
              <ul class="nav nav-second-level collapse">
                <li>
                  <a href="resolutions.php">Resolutions</a>
                </li>
              </ul>
            </li> -->
            <li>
              <a href="#"><i class="fa fa-gear fa-fw"></i> Settings<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                <li>
                  <a href="procedures.php">Procedures</a>
                </li>
                <li>
                  <a href="offenses.php">Offenses</a>
                </li>
                <li>
                  <a href="status.php"></i> Status</a>
                </li>
                <li>
                  <a href="prosecutor.php">Users</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
 <div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header"><?php echo $pagename; ?></h1>
    </div>
  </div>