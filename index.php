<?php
$pagename='Dashboard';
include_once('header.php');
// $user = new Prosecutor();
$user = new User();
if(Session::exists('success')){
  echo '<p>' . Session::flash('success') . '</p>';
}
// To check for user access/permission
// if ($user->hasPermission('admin')){
//     echo '<p>You are an admin!</p>';
//   }

?>
<div class="row">
  <div class="col-lg-8">
    <div class="panel panel-primary">
      <div class="panel-heading">Prosecutors</div>
      <div class="panel-body">
        <table id="caseList" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Case ID</th>
              <th>NPS Docket No.</th>
              <th>Complainant</th>
              <th>Respondent</th>
              <th>Offense</th>
              <th>Prosecutor</th>
              <th>Resolution</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>12-123</td>
              <td>Complainant 1</td>
              <td>Respondent 1</td>
              <td>Offense 1</td>
              <td>Prosecutor 1</td>
              <td><a href="#">file.pdf</a></td>
              <td>Resolution file</td>
            </tr>
          </tbody>
        </table>
      </div>
      </div>
  </div>
  <div class="col-lg-4">
    <div class="panel panel-primary">
      <div class="panel-heading">calendar view</div>
      <div class="panel-body">
        
      </div>
    </div>
  </div>
</div>

  <?php include 'footer.php'; ?>
  <script>
  $(document).ready(function() {
    $('#caseList').DataTable({
      responsive: true
    });
  });
  </script>
</body>
</html>