<?php
$pagename='Dashboard';
include_once('header.php');
// $user = new Prosecutor();
$user = new User();
if(Session::exists('success')){
  echo '<p>' . Session::flash('success') . '</p>';
}
?>

<div class="row">
  <div class="col-xs-2 col-xs-offset-5">
    <h4><span class="h1" style="background-color: red; padding: 3px; border-radius: 3px;">23</span></h4>
    <h6>APRIL</h6>
  </div>
    <div class="col-xs-12">
      <div class="panel panel-danger">
      <div class="panel-heading">
        <h4 class="text-uppercase"><strong>PP vs Person 1, Person 2, Person 3, Person 4</strong></h4>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Monday</li>
          <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 12:30 PM - 2:00 PM</li>
          <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cafe</li>
        </ul></div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Action</td>
            <td>Date</td>
            <td>Status</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Rescheduled</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Complete</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td><select class="form-control" name="" id="">
              <option value="">1</option>
              <option value="">2</option>
              <option value="">reschedule</option>
            </select></td>
          </tr>
        </table>
        <button class="btn btn-primary pull-right">Confirm</button>
      </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="panel panel-danger">
      <div class="panel-heading">
        <h4 class="text-uppercase"><strong>PP vs Person 1, Person 2, Person 3, Person 4</strong></h4>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Monday</li>
          <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 12:30 PM - 2:00 PM</li>
          <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cafe</li>
        </ul></div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Action</td>
            <td>Date</td>
            <td>Status</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Rescheduled</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Complete</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td><select class="form-control" name="" id="">
              <option value="">1</option>
              <option value="">2</option>
              <option value="">reschedule</option>
            </select></td>
          </tr>
        </table>
        <button class="btn btn-primary pull-right">Confirm</button>
      </div>
      </div>
    </div>
</div>

  <?php include 'footer.php'; ?>
</body>
</html>