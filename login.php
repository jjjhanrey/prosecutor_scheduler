<!DOCTYPE html>
<html>
<head>

  <?php
  require_once('core/init.php');
  $pagename='Login';
  $user = new User();
  ?>
  <title><?php echo $pagename; ?></title>
  <?php require_once('core/init.php'); ?>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
  <link href="assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/DataTables/Responsive-2.2.1/css/responsive.bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/metisMenu/metisMenu.min.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendors/select2/select2.css" />
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <!-- <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> -->
        <a class="navbar-brand" href="index.php">Prosecutor Scheduler</a>
      </div>
    </nav>
  </div>
  <div class="col-lg-4 col-lg-offset-4" style="margin-top: 10%;">
    <div class="panel panel-primary">
      <div class="panel-heading">Login</div>
      <div class="panel-body">
        <?php if(Session::exists('login')){
          echo '
            <div class="alert alert-success" role="alert">
              '. Session::flash('login') .'
            </div>
          ' ;
        }
          if (Input::exists()) {
    if (Token::check(Input::get('token'))) {
      $validate = new Validate();
      $validation = $validate->check($_POST,array(
        'username' => array('required' => true),
        'password' => array('required' => true)
      ));

      if ($validation->passed()) {
        $user = new User();
        $remember = (Input::get('remember') === 'on') ? true : false;
        $login = $user->login(Input::get('username'), Input::get('password'), $remember);
        if ($login) {
          Redirect::to('index.php');
          Session::flash('index','Succesfully logged in.');
        } else
          echo '
            <div class="alert alert-danger" role="alert">
              Invalid username and password combination.
            </div>
          ';
      } else{
        echo '<div class="alert alert-danger" role="alert">';
        foreach($validation->errors() as $error){
          echo $error, '<br>';
        // Session::flash('register', $error);
        }
        echo '</div>';
      }
    }
  }
        ?>
        <form action="" method="post" name="userAdd" id="loginForm">
          <div class="form-group">
            <label>Username</label>
            <input class="form-control" name="username" placeholder="" type="text" value="<?php echo escape(Input::get('username')); ?>">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input class="form-control" name="password" placeholder="" type="password" value="">
          </div>
          <div class="form-group">
            <label> <input type="checkbox" name="remember"> Remember me </label>
          </div>
          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
          <button class="btn btn-primary btn-block" type="submit">Submit</button>
        </form>  
      </div>
     </div>
  </div>
<?php include_once('footer.php'); ?>

<script>
    $(document).ready(function() {
    $('#loginForm').formValidation({
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        username: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            },
            remote: {
                type: 'POST',
                url: 'scripts/taken_username.php',
                message: 'Username does not exist.',
                delay: 500
            }

          }
        },
        password: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        }
      }
    });
    });
</script>