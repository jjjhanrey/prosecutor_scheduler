<div class="modal fade" id="editModal" role="dialog" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <form action='scripts/update.php' method='post' name="" id="">
        <div class="modal-body" id="infoEdit"></div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
          <button class="btn btn-primary" type="submit" name="theSubmit">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" role="dialog" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action='scripts/delete.php' method='post'>
        <div class="modal-header">
          <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body" id="infoDelete"></div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
          <button class="btn btn-danger" name="submit" type="submit">Yes, delete</button>
        </div>
      </form>
    </div>
  </div>
</div>