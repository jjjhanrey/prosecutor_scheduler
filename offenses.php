<?php
$pagename='Offenses';
include_once('header.php');
$setting = new Offenses();
?>
    <div class="row">
      <div class="col-lg-8">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Offenses
          </div>
          <div class="panel-body">
            <div class="panel-body">
              <table cellspacing="0" class="table table-striped table-bordered" id="example" width="100%">
                <thead>
                  <tr>
                    <th>Offense</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
              <?php  echo $setting->getList();?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><!-- Panel lg-8 end -->
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Add Offenses
          </div>
          <div class="panel-body">
            <form action="scripts/add_offense.php" id="offenseAdd" method="post" name="offenseAdd">
              <div class="form-group">
                <label>Offense</label> <input class="form-control" name="offense" placeholder="" type="text">
              </div><button class="btn btn-primary btn-block" name="theSubmit" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div><!-- Panel lg-4 end -->
      <div aria-labelledby="myModalLabel" class="modal fade" id="editOffense" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Edit Offense</h4>
            </div>
            <form action='scripts/edit_offense.php' id="offenseForm" method='post' name="offenseForm">
              <div class="modal-body" id="offenseInfo"></div>
              <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button> <button class="btn btn-primary" name="submitEdit" type="submit">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div><!-- delete -->
      <div aria-labelledby="myModalLabel" class="modal fade" id="deleteOffense" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form action='scripts/delete_offense.php' method='post'>
              <div class="modal-header">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirm delete</h4>
              </div>
              <div class="modal-body" id="offenseInfoDelete">
                <!-- Are you sure you want to delete ... -->
              </div>
              <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button> <button class="btn btn-danger" name="submitDelete" type="submit">Yes, delete</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  

<?php include 'footer.php'; ?>
<?php include 'modal.php'; ?>



  <script>
  $(document).ready(function() {
    $('#example').DataTable();
    
    $('.delete').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'delete',
          page: 'offense'
        },
        success: function(data) {
          $('#infoDelete').html(data);
        }
      });
      $('#deleteModal').modal("show");
    });
    $('.edit').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'edit',
          page: 'offense'
        },
        success: function(data) {
          $('#infoEdit').html(data);
        }
      });
      $('#editModal').modal("show");
    });

    $('#offenseAdd').formValidation({
      excluded: ':disabled',
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        offense: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        }
      }
    });
    $('#editOffense').on('hide.bs.modal', function() {
      $('#offenseForm').data('formValidation').destroy();
    });
    // Fix the bug
    $('#offenseForm').on("keyup keypress", function(e) {
      var code = e.keyCode || e.which;
      if (code === 13) {
        e.preventDefault();
        $("button[name='submitEdit']").click();
        return false;
      }
    });
    // Fix the bug
    $('#offenseAdd').on("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      $("button[name='theSubmit']").click();
      return false;
    }
    });
    });
  </script>
