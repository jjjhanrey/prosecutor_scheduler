<?php
$pagename='Procedures';
include_once('header.php');
$setting = new Procedures();
?>
    <div class="row">
      <div class="col-lg-8">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Procedures
          </div>
          <div class="panel-body">
            <div class="panel-body">
              <table cellspacing="0" class="table table-striped table-bordered" id="example" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Procedure</th>
                  </tr>
                </thead>
                <tbody>
              <?php  echo $setting->getList();?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><!-- Panel lg-8 end -->
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Add Procedures
          </div>
          <div class="panel-body">
            <form method="POST" action="scripts/add_procedure.php">
              <div class="form-group">
                <label>Procedures</label>
                <input class="form-control" type="text" name="proceduretaken">
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="theSubmit">Submit</button>
            </form>
          </div>
        </div>
      </div><!-- Panel lg-4 end -->
    </div>
  </div>
<?php include_once('modal.php'); ?>

  <?php include 'footer.php'; ?> 



  
  <script>
  $(document).ready(function() {
    $('#example').DataTable();

    $('.delete').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'delete',
          page: 'procedure'
        },
        success: function(data) {
          $('#infoDelete').html(data);
        }
      });
      $('#deleteModal').modal("show");
    });
    $('.edit').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'edit',
          page: 'procedure'
        },
        success: function(data) {
          $('#infoEdit').html(data);
        }
      });
      $('#editModal').modal("show");
    });

  });

  </script>