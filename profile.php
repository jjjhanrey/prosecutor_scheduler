<?php
$pagename='Profile';
include_once('header.php');
?>
<div class="row">
  <div class="col-lg-6">
    <div class="panel panel-primary">
      <div class="panel-heading">Profile</div>
      <div class="panel-body">
        <form action="scripts/update_profile.php" method="POST">
        <?php 
          $user = new User();
          echo '
            <div class="form-group">
              <label>Last Name</label>
              <input type="text" class="form-control" name="username" placeholder="" value="'.escape($user->data()->username).'" disabled>
            </div>';
          echo '
            <div class="form-group">
              <label>Last Name</label>
              <input type="text" class="form-control" name="lastname" placeholder="" value="'.escape($user->data()->firstname).'">
            </div>';
          echo '
            <div class="form-group">
              <label>First Name</label>
              <input type="text" class="form-control" name="firstname" placeholder="" value="'.escape($user->data()->lastname).'">
            </div>';
          echo '
            <div class="form-group">
              <label>Middle Name</label>
              <input type="text" class="form-control" name="middlename" placeholder="" value="'.escape($user->data()->middlename).'">
            </div>';
          echo '
            <div class="form-group">
              <label>Gender</label>
              <select class="form-control" name="gender">';
                if (escape($user->data()->gender)=='0'){
                  echo "
                    <option value='0' selected> Male</option>
                    <option value='1'> Female</option>
                  ";
                } else 
                  echo "
                    <option value='0'> Male</option>
                    <option value='1' selected> Female</option>
                  ";
          echo '
              </select>
            </div>';
          
         ?>
          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
          <button class="btn btn-primary btn-block" type="submit">Submit</button>
         </form>
      </div>
    </div>
  </div> <!-- Panel lg-8 end -->

<!--   <div class="col-lg-4">
    <div class="panel panel-primary">
      <div class="panel-heading">Add Prosecutor</div>
      <div class="panel-body">
      
      </div>
     </div>
      </div> -->
</div>

<?php include_once('footer.php'); ?>