<?php
$pagename='Users';
include_once('header.php');
$user = new User();


?>
<?php

 if(Session::exists('prosecutor_error')){
  echo '
    <div class="alert alert-danger" role="alert">
      '. Session::flash('prosecutor_error') .'
    </div>
  ' ;
} else if (Session::exists('prosecutor_success')) {
    echo '
      <div class="alert alert-success" role="alert">
    '.Session::flash('prosecutor_success').'
      </div>
    ';
  }
  ?>
  <div class="row">
    <div class="col-lg-8	">
      <div class="panel panel-primary">
        <div class="panel-heading">Prosecutors</div>
        <div class="panel-body">
          <div class="table-responsive">
           <table cellspacing="0" class="table table-striped table-bordered" id="tbl_show" width="100%">
              <thead>
                <tr>
                  <th data-priority="1">Username</th>
                  <th data-priority="3">Name</th>
                  <th data-priority="2">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php  echo $user->getList();?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div> <!-- Panel lg-8 end -->

  <div class="col-lg-4">
    <div class="panel panel-primary">
      <div class="panel-heading">Add Prosecutor</div>
      <div class="panel-body">
      <form action="scripts/add_user.php" method="post" name="userAdd" id="userAdd">
        <div class="form-group">
            <label>Username</label>
            <input class="form-control" name="username" placeholder="" type="text" value="<?php echo escape(Input::get('username')); ?>">
          </div>
          <div class="form-group">
            <label>Access Level</label>
            <select class="form-control" name="permission">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
            
          </div>
          <div class="form-group">
            <label>Password</label>
            <input class="form-control" name="password" placeholder="" type="text" value="">
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
            <input class="form-control" name="confirmPassword" placeholder="" type="text" value="">
          </div>
          <div class="form-group">
            <label>Last Name</label>
            <input class="form-control" name="lastname" type="text" value="<?php echo escape(Input::get('lastname')); ?>">
          </div>
          <div class="form-group">
            <label>First Name</label>
            <input class="form-control" name="firstname" placeholder="" type="text" value="<?php echo escape(Input::get('firstname')); ?>">
          </div>
          <div class="form-group">
            <label>Middle Name</label>
            <input class="form-control" name="middlename" placeholder="" type="text" value="<?php echo escape(Input::get('middlename')); ?>">
          </div>
          <!-- <div class="form-group">
            <label>Gender</label>
            <select class="form-control" name="gender">
              <option disabled selected>-Select Gender-</option>
              <option value="0">Male</option>
              <option value="1">Female</option>
            </select>
          </div> -->
          
          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">

          <button class="btn btn-primary btn-block" name="submitAdd" type="submit">Submit</button>
        </form>
      </div>
     </div>
      </div>
</div>
<?php include_once('modal.php'); ?>

<?php include_once('footer.php'); ?>
<script>
  $(document).ready(function() {
    // Datables initialize
    $('#tbl_show').DataTable({
      responsive:true
    });
    $('.delete').click(function() {
      console.log('delete');
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'delete',
          page: 'user'
        },
        success: function(data) {
          $('#infoDelete').html(data);
        }
      });
      $('#deleteModal').modal("show");
    });
    $('.edit').click(function() {
      console.log('edit');
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'edit',
          page: 'user'
        },
        success: function(data) {
          $('#infoEdit').html(data);
        }
      });
      $('#editModal').modal("show");
    });
    $('#userAdd').formValidation({
            message: 'This value is not valid',
            icon: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
              username: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  }
                },
                stringLength: {
                  min: 6,
                  message: 'Username too short.'
                }
              },
              password: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  },
                  stringLength: {
                    min: 6,
                    message: 'Password too short.'
                  }
                }
              },
              confirmPassword: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  },
                  identical: {
                    field: 'password',
                    message: 'Passwords do not match.'
                  }
                }
              },
              lastname: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  }
                }
              },
              firstname: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  }
                }
              },
              middlename: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  }
                }
              },
              gender: {
                validators: {
                  notEmpty: {
                    message: 'Select gender.'
                  }
                }
              },
              permission: {
                validators: {
                  notEmpty: {
                    message: 'This is a required field.'
                  }
                }
              }
            }
          });
  });
</script>