<?php
$pagename='Reports';
include_once('header.php');
$setting = new Prosecutor();
?>
        <h3 class="text-danger">This is a static page.</h3>
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">Reports</div>
          <div class="panel-body">
            <div class="panel-body">
              <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Case ID</th>
                    <th>NPS Docket No.</th>
                    <th>Complainant</th>
                    <th>Respondent</th>
                    <th>Offense</th>
                    <th>Prosecutor</th>
                    <th>Resolution</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>12-123</td>
                    <td>Complainant 1</td>
                    <td>Respondent 1</td>
                    <td>Offense 1</td>
                    <td>Prosecutor 1</td>
                    <td>Resolution file</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> <!-- Panel lg-8 end -->
      <!-- <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">Add Case</div>
          <div class="panel-body">
            <form class="form-horizontal" id="cases" name="cases">
              <div class="form-group">
                <label class="col-sm-2 control-label">NPS Docket No.</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="npsDocket">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Complainant</label>
                <div class="col-sm-3">
                  <select class="form-control">
                    <option selected disabled>-Select Complainant-</option>
                    <option value="Complainant 1">Complainant 1</option>
                    <option value="Complainant 2">Complainant 2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Respondent</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Offense</label>
                <div class="col-sm-3">
                  <select class="form-control">
                    <option selected disabled>-Select Offense-</option>
                    <option value="Offense 1">Offense 1</option>
                    <option value="Offense 2">Offense 2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Complainant</label>
                <div class="col-sm-3">
                  <select class="form-control">
                    <option selected disabled>-Select Offense-</option>
                    <option value="Complainant 1">Complainant 1</option>
                    <option value="Complainant 2">Complainant 2</option>
                  </select>
                </div>
              </div>
                  <div class="form-group">
        <label class="col-xs-3 control-label">Browser</label>
        <div class="col-xs-5">
            <select class="form-control" name="browsers" multiple id="select">
                <option value="chrome">Google Chrome</option>
                <option value="firefox">Firefox</option>
                <option value="ie">IE</option>
                <option value="safari">Safari</option>
                <option value="opera">Opera</option>
                <option value="other">Other</option>
            </select>
        </div>
    </div>
    <button class="btn btn-primary" type="submit">Submit</button>
</form>
          </div>
        </div>
      </div> --> <!-- Panel lg-4 end -->

    </div>
  </div>
  </div>
  <?php include 'includes/footer.php'; ?>
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
  });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
        $('#select').multiselect({
           enableFiltering: true,
           enableCaseInsensitiveFiltering: true
        });
        $('#cases').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 npsDocket: {
                    validators: {
                        notEmpty: {
                            message: 'This is a required field.'
                        },
                        regexp: {
                            regexp: /^([0-9])|^([0-9.]+\d{2})/,
                            message: 'Invalid character detected.'
                        }
                    }
                }       
            }
        });
    });
</script>
</body>

</html>