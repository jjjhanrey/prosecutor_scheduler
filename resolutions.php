<?php 
$pagename='Resolutions';
include 'includes/header.php';
?>
    <div class="row">
      <div class="col-lg-8">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Procedures
          </div>
          <div class="panel-body">
            <div class="panel-body">
              <table cellspacing="0" class="table table-striped table-bordered" id="example" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Procedure</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Procedure 1</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><!-- Panel lg-8 end -->
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Add Procedures
          </div>
          <div class="panel-body">
            <form>
              <div class="form-group">
                <label>Procedures</label> <input class="form-control" id="" placeholder="" type="text">
              </div><button class="btn btn-primary btn-block" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div><!-- Panel lg-4 end -->
    </div>
  </div>
  <?php include 'includes/footer.php'; ?> 



  
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
  });
  </script>