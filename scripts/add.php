<?php 
include_once('../core/init2.php');
$user= new User();

if (!empty($_POST)) {
	$user->create(array(
		'access' => Input::get('access'),
		'lastname' => Input::get('lastname'),
		'firstname' => Input::get('firstname'),
		'middlename' => Input::get('middlename'),
		'gender' => Input::get('gender')
	));
	echo "OK";
}
?>