<?php 
include_once('../core/init2.php');
$case = new Cases();
if (Input::exists()){
  if (Token::check(Input::get('token'))) {
    $validate = new Validate();
    $validation = $validate->check($_POST,array(
      'npsDocket' => array(
        'required' => true
        // 'min' => 2,
        // 'max' => 20
      ),
      "complainant" => array(
        'required' => true
      ),
      "respondent" => array(
        'required' => true
      ),
      "offense" => array(
        'required' => true
      ),
      "prosecutor" => array(
        'required' => true
      ),
      // "actiontaken" => array(
      //   'required' => true
      // ),
      "resolution" => array(
        'required' => true
      )
    )); 

    if ($validation->passed()) {
       // echo "passed";
      // Session::flash('success','You have registered successfully.');
      //   header('Location: index.php');
      try{
        $case->create(array(
          'npsDocket'=> Input::get('npsDocket'),
          'complainant_id'=> Input::get('complainant'),
          'respondent'=> Input::get('respondent'),
          'offense_id'=> Input::get('offense'),
          'prosecutor_id'=> Input::get('prosecutor'),
          'resolution_id'=> Input::get('resolution')
        ));
        Session::flash('case_success','Case added successfully.');
        Redirect::to('../cases.php');
        

      } catch(Exception $e){
          die($e->getMessage());
      }
     } else {
        foreach($validation->errors() as $error){
          $error1 .= $error . '<br>';
          // echo $error, '<br>';
        Session::flash('case_error',$error1);
     Redirect::to('../cases.php');

        }

     // Redirect::to('../prosecutor.php');
     }
  }
}
 ?>