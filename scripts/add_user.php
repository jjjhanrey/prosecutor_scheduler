<?php 
include_once('../core/init2.php');
$user = new User();
if (Input::exists()){
  if (Token::check(Input::get('token'))) {
    $validate = new Validate();
    $validation = $validate->check($_POST,array(
      'username' => array(
        'required' => true, 
        'min' => 2,
        'max' => 20,
        'unique' => 'tbl_users'
      ),
      // 'gender' => array(
      //   'required' => true
      // ),
      "password" => array(
        'required' => true,
        'min' => 6
      )
      ,
      'confirmPassword' => array(
        'required' => true,
        'matches' => 'password'
      ),
      'lastname' => array(
        'required' => true
      ),
      'firstname' => array(
        'required' => true
      ),
      'password' => array(
        'required' => true
      ),
      // 'gender' => array(
      //   'required' => true
      // )
    )); 

    if ($validation->passed()) {
       // echo "passed";
      // Session::flash('success','You have registered successfully.');
      //   header('Location: index.php');
      $user=new Prosecutor();

      $salt = Hash::salt(32);
      // echo $salt;
      // die();
      try{
        $user->create(array(
          'username'=> Input::get('username'),
          'password'=> Hash::make(Input::get('password'), $salt),
          'salt'=> $salt,
          'permission'=> Input::get('permission'),
          'lastname'=> Input::get('lastname'),
          'firstname'=> Input::get('firstname'),
          'middlename'=> Input::get('middlename'),
          'gender'=> Input::get('gender')
        ));

        Session::flash('prosecutor_success','Added successfully. Account is now ready to be used.');
        Redirect::to('../prosecutor.php');
        

      } catch(Exception $e){
          die($e->getMessage());
      }
     } else {
        foreach($validation->errors() as $error){
          echo $error, '<br>';
        Session::flash('prosecutor_error',$error);
     Redirect::to('../prosecutor.php');

        }

     // Redirect::to('../prosecutor.php');
     }
  }
}
 ?>