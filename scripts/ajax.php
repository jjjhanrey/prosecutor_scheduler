<?php 
require_once '../core/init2.php';

if (isset($_POST['id']) && $_POST['page']=='user' && $_POST['type']=='edit') {
$id=$_POST['id'];
$user = new User($id);
echo '
	<div class="form-group">
  	<label>Last Name</label>
  	<input type="hidden" class="form-control" name="id_user" placeholder="" value="'.$id.'">
  	<input type="text" class="form-control" name="lastname" placeholder="" value="'.escape($user->data()->firstname).'">
	</div>';
echo '
	<div class="form-group">
  	<label>First Name</label>
  	<input type="text" class="form-control" name="firstname" placeholder="" value="'.escape($user->data()->lastname).'">
	</div>';
echo '
	<div class="form-group">
  	<label>Middle Name</label>
  	<input type="text" class="form-control" name="middlename" placeholder="" value="'.escape($user->data()->middlename).'">
	</div>';
echo '
	<div class="form-group">
  	<label>Gender</label>
  	<select class="form-control" name="gender">';
  		if (escape($user->data()->gender)=='0'){
		  	echo "
					<option value='0' selected> Male</option>
    			<option value='1'> Female</option>
		  	";
		  } else 
		  	echo "
					<option value='0'> Male</option>
    			<option value='1' selected> Female</option>
		  	";
echo '
  	</select>
	</div>';
} else if (isset($_POST['id']) && $_POST['page']=='user' && $_POST['type']=='delete') {
	$id=$_POST['id'];
	$user = new User($id);
	echo "Are you sure you want to delete <b>".escape($user->data()->lastname). 
	", " . escape($user->data()->firstname)."</b>?";
	echo"<input class='hidden' name='id_user' value='".$id."'></input>";
}


// procedure
 else if (isset($_POST['id']) && $_POST['page']=='procedure' && $_POST['type']=='edit'){
	$id=$_POST['id'];
	$procedure = new Procedures($id);
	echo '
		<div class="form-group">
	  	<label>Procedure</label>
	  	<input type="hidden" class="form-control" name="id_procedure" placeholder="" value="'.$id.'">
	  	<input type="text" class="form-control" name="procedure" placeholder="" value="'.escape($procedure->data()->proceduretaken).'">
		</div>';
	} else if (isset($_POST['id']) && $_POST['page']=='procedure' && $_POST['type']=='delete'){
	$id=$_POST['id'];
	$procedure = new Procedures($id);
	echo "Are you sure you want to delete <b>".escape($procedure->data()->proceduretaken)."</b>?";
	echo"<input class='hidden' name='id_procedure' value='".$id."'></input>";
	}

// Offenses
	else if (isset($_POST['id']) && $_POST['page']=='offense' && $_POST['type']=='edit'){
	$id=$_POST['id'];
	$offenses = new Offenses($id);
	echo '
		<div class="form-group">
	  	<label>Offense</label>
	  	<input type="hidden" class="form-control" name="id_offense" placeholder="" value="'.$id.'">
	  	<input type="text" class="form-control" name="offense" placeholder="" value="'.escape($offenses->data()->offense).'">
		</div>';
	} 

	else if (isset($_POST['id']) && $_POST['page']=='offense' && $_POST['type']=='delete') {
	$id=$_POST['id'];
	$offenses = new Offenses($id);
	echo "Are you sure you want to delete <b>".escape($offenses->data()->offense)."</b>?";
	echo"<input class='hidden' name='id_offense' value='".$id."'></input>";
}


// Status
	else if (isset($_POST['id']) && $_POST['page']=='status' && $_POST['type']=='edit'){
	$id=$_POST['id'];
	$status = new Status($id);
	echo '
		<div class="form-group">
	  	<label>Status</label>
	  	<input type="hidden" class="form-control" name="id_status" placeholder="" value="'.$id.'">
	  	<input type="text" class="form-control" name="status" placeholder="" value="'.escape($status->data()->status).'">
		</div>';
	} 

	else if (isset($_POST['id']) && $_POST['page']=='status' && $_POST['type']=='delete') {
	$id=$_POST['id'];
	$status = new Status($id);
	echo "Are you sure you want to delete <b>".escape($status->data()->status)."</b>?";
	echo"<input class='hidden' name='id_status' value='".$id."'></input>";
}

// complainant
if (isset($_POST['id']) && $_POST['page']=='complainant' && $_POST['type']=='edit') {
$id=$_POST['id'];
$complainant = new Complainants($id);
echo '
	<div class="form-group">
  	<label>Last Name</label>
  	<input type="hidden" class="form-control" name="id_complainant" placeholder="" value="'.$id.'">
  	<input type="text" class="form-control" name="lastname" placeholder="" value="'.escape($complainant->data()->firstname).'">
	</div>';
echo '
	<div class="form-group">
  	<label>First Name</label>
  	<input type="text" class="form-control" name="firstname" placeholder="" value="'.escape($complainant->data()->lastname).'">
	</div>';
echo '
	<div class="form-group">
  	<label>Middle Name</label>
  	<input type="text" class="form-control" name="middlename" placeholder="" value="'.escape($complainant->data()->middlename).'">
	</div>';
// echo '
// 	<div class="form-group">
//   	<label>Gender</label>
//   	<select class="form-control" name="gender">';
//   		if (escape($complainant->data()->gender)=='0'){
// 		  	echo "
// 					<option value='0' selected> Male</option>
//     			<option value='1'> Female</option>
// 		  	";
// 		  } else 
// 		  	echo "
// 					<option value='0'> Male</option>
//     			<option value='1' selected> Female</option>
// 		  	";
echo '
  	</select>
	</div>';
} else if (isset($_POST['id']) && $_POST['page']=='complainant' && $_POST['type']=='delete') {
	$id=$_POST['id'];
	$complainant = new Complainants($id);
	echo "Are you sure you want to delete <b>".escape($complainant->data()->lastname). 
	", " . escape($complainant->data()->firstname)."</b>?";
	echo"<input class='hidden' name='id_complainant' value='".$id."'></input>";
}
?>