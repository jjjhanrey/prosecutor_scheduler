<?php 
include_once('../core/init2.php');

if (Input::exists()) {
	if (Token::check(Input::get('token'))) {
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
					'oldPW'=> array(
							'required'=>true,
							'min'=>6
							),
					'newPW'=> array(
							'required'=>true,
							'min'=>6
							),
					'confirmPW'=> array(
							'required'=>true,
							'min'=>6,
							'matches'=>'newPW'
							)
			));

		if($validation->passed()){
			$user = new User();

			if(Hash::make(Input::get('oldPW'), $user->data()->salt) !== $user->data()->password){
				Session::flash('changepassword','Incorrect old password.');
				Redirect::to('../myaccount.php');
			} else {
				echo Input::get('newPW');
				echo "try update";
				$salt= Hash::salt(32);
				$user->update(array(
					'password' => Hash::make(Input::get('newPW'),$salt),
					'salt' => $salt
					));

				Session::flash('changepassword','Your pasword has been changed.');
				Redirect::to('../myaccount.php');
			}

		} else {
			echo "ERROR";
			foreach($validation->errors() as $error){
				echo $error, '<br>';
			}
		}
	}
}

 ?>