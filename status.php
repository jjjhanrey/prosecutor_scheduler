<?php
$pagename='Status';
include_once('header.php');
$setting = new Status();
?>
    <div class="row">
      <div class="col-lg-8">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Status
          </div>
          <div class="panel-body">
            <div class="panel-body">
              <table cellspacing="0" class="table table-striped table-bordered" id="example" width="100%">
                <thead>
                  <tr>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
              <?php  echo $setting->getList();?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><!-- Panel lg-8 end -->
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Add Status
          </div>
          <div class="panel-body">
            <form action="scripts/add_status.php" id="statusAdd" method="post" name="statusAdd">
              <div class="form-group">
                <label>Status</label> <input class="form-control" name="status" placeholder="" type="text">
              </div><button class="btn btn-primary btn-block" name="theSubmit" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div><!-- Panel lg-4 end -->
    </div>
  </div>
  

<?php include 'footer.php'; ?>
<?php include 'modal.php'; ?>



  <script>
  $(document).ready(function() {
    $('#example').DataTable();
    
    $('.delete').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'delete',
          page: 'status'
        },
        success: function(data) {
          $('#infoDelete').html(data);
        }
      });
      $('#deleteModal').modal("show");
    });
    $('.edit').click(function() {
      var id = $(this).attr('id');
      $.ajax({
        url: "scripts/ajax.php",
        method: "post",
        data: {
          id: id,
          type: 'edit',
          page: 'status'
        },
        success: function(data) {
          $('#infoEdit').html(data);
        }
      });
      $('#editModal').modal("show");
    });

    $('#statusAdd').formValidation({
      excluded: ':disabled',
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        offense: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        }
      }
    });
    $('#editOffense').on('hide.bs.modal', function() {
      $('#offenseForm').data('formValidation').destroy();
    });
    // Fix the bug
    $('#offenseForm').on("keyup keypress", function(e) {
      var code = e.keyCode || e.which;
      if (code === 13) {
        e.preventDefault();
        $("button[name='submitEdit']").click();
        return false;
      }
    });
    // Fix the bug
    $('#offenseAdd').on("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      $("button[name='theSubmit']").click();
      return false;
    }
    });
    });
  </script>
