<?php
$pagename='Dashboard';
include_once('header.php');
include_once('scripts/auth.php');
$setting = new Prosecutor();
?>
<style>
.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
}
    .clickable {
      cursor: pointer;
    }
    .clickable .glyphicon {
      background: rgba(0, 0, 0, 0.15);
      display: inline-block;
      padding: 6px 12px;
      border-radius: 4px
    }
    .panel-heading span {
      margin-top: -23px;
      font-size: 15px;
      margin-right: -9px;
    }
    .panel-heading button  {
      margin-top: -25px;
    }
</style>
<div class="panel panel-primary">
          <div class="col-xs-2 col-xs-offset-5">
        <h4><span class="h1" style="background-color: red; padding: 3px; border-radius: 3px;">23</span></h4>
        <h6>---</h6>
      </div>
<div class="col-xs-12">
      <div class="panel panel-danger">
      <div class="panel-heading">        <h3 class="text-uppercase"><strong>Ice Cream Social</strong></h3>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Monday</li>
          <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 12:30 PM - 2:00 PM</li>
          <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cafe</li>
        </ul></div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Action</td>
            <td>Date</td>
            <td>Status</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Rescheduled</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Complete</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td><select class="form-control" name="" id="">
              <option value="">1</option>
              <option value="">2</option>
              <option value="">reschedule</option>
            </select></td>
          </tr>
        </table>
        <button class="btn btn-primary pull-right">Confirm</button>
      </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="panel panel-success">
      <div class="panel-heading">        <h4 class="text-uppercase">PP vs Jhan Rey Gonzales, Teresa S Lazaro, Junife April Reganit Jimenez</h4>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Monday</li>
          <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 12:30 PM - 2:00 PM</li>
          <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cafe</li>
          <li class="list-inline-item pull-right"><a href=""><i class="fa fa-location-arrow" aria-hidden="true"></i>More</li></a>

        </ul></div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Action</td>
            <td>Date</td>
            <td>Status</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Rescheduled</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Complete</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Pending</td>
          </tr>
        </table>
      </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="panel panel-primary">
      <div class="panel-heading">        <h3 class="text-uppercase"><strong>PP vs Jhan Rey Gonzales, Teresa S Lazaro, Junife April Reganit Jimenez</strong></h3>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Monday</li>
          <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 12:30 PM - 2:00 PM</li>
          <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cafe</li>
        </ul></div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Action</td>
            <td>Date</td>
            <td>Status</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Rescheduled</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Complete</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Pending</td>
          </tr>
        </table>
      </div>
      </div>
    </div>


    <!-- <div class="row row-striped"> -->
      
<!-- panel-group -->
  
  
</div><!-- container -->
  </div>
    </div>
    
    </div><!-- Panel lg-4 end -->
  </div>

  <?php include 'footer.php'; ?>

  <script>
  $(document).ready(function() {

    $('#caseAdd')
        .find('[name="complainants[]"]')
            .select2()
            // Revalidate the color when it is changed
            .change(function(e) {
                $('#caseAdd').formValidation('revalidateField', 'complainants[]');
            })
            .end().formValidation({
      excluded: ':disabled',
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        npsDocket: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        },
        'complainants[]': {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        },
        resolution: {
        validators: {
            file: {
                extension: 'pdf',
                type: 'application/pdf',
                maxSize: 10*1024*1024,
                message: 'Please choose a pdf file with a size less than 10M.'
            }
        }
    }
      }
    });
    $('select.selecta').select2({
  placeholder: 'Select an option'
});
    $('#example').DataTable();
  });
  </script>
</body>
</html>