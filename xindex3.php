<?php
$pagename='Dashboard';
include_once('header.php');
$user = new User();
if(Session::exists('success')){
  echo '<p>' . Session::flash('success') . '</p>';
}

if ($user->hasPermission('admin')){
    echo '<p>You are a moderator!</p>';
  }

?>
  <div class="col-xs-2 col-xs-offset-5">
    <h4><span class="h1" style="background-color: red; padding: 3px; border-radius: 3px;">23</span></h4>
    <h6>---</h6>
  </div>
    <div class="col-xs-12">
      <div class="panel panel-danger">
      <div class="panel-heading">F
        <h4 class="text-uppercase"><strong>PP vs Person 1, Person 2, Person 3, Person 4</strong></h4>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Monday</li>
          <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 12:30 PM - 2:00 PM</li>
          <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cafe</li>
        </ul></div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Action</td>
            <td>Date</td>
            <td>Status</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Rescheduled</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td>Complete</td>
          </tr>
          <tr>
            <td>sda</td>
            <td>January 1, 2018</td>
            <td><select class="form-control" name="" id="">
              <option value="">1</option>
              <option value="">2</option>
              <option value="">reschedule</option>
            </select></td>
          </tr>
        </table>
        <button class="btn btn-primary pull-right">Confirm</button>
      </div>
      </div>
    </div>
  <?php include 'footer.php'; ?>

  <script>
  $(document).ready(function() {
    $('#caseAdd')
        .find('[name="complainants[]"]')
            .select2()
            // Revalidate the color when it is changed
            .change(function(e) {
                $('#caseAdd').formValidation('revalidateField', 'complainants[]');
            })
            .end().formValidation({
      excluded: ':disabled',
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        npsDocket: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        },
        'complainants[]': {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        },
        resolution: {
        validators: {
            file: {
                extension: 'pdf',
                type: 'application/pdf',
                maxSize: 10*1024*1024,
                message: 'Please choose a pdf file with a size less than 10M.'
            }
        }
    }
      }
    });
    $('select.selecta').select2({
  placeholder: 'Select an option'
});
    $('#example').DataTable();
  });
  </script>
</body>
</html>