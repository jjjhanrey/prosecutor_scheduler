<?php
$pagename='Dashboard';
include 'includes/header.php';
?>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="index.php">Prosecutor Scheduler</a>
      </div>
      <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user fa-fw white"></i> <i class="fa fa-caret-down white"></i></a>
          <ul class="dropdown-menu dropdown-user">
            <li>
              <a href="#"><i class="fa fa-user fa-fw"></i> User Profile </a>
            </li>
            <li>
              <a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul>
        </li>
      </ul>
      <div class="navbar-default sidebar" role="navigation">
        <div class="collapse sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">
            <li>
              <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
              <a href="cases.php"><i class="fa fa-dashboard fa-fw"></i> Cases</a>
            </li>
            <li>
              <a href="reports.php"><i class="fa fa-dashboard fa-fw"></i> Reports</a>
            </li>
            <li>
              <a href="#"><i class="fa fa-gear fa-fw"></i> Settings<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level collapse">
                <li>
                  <a href="complainants.php"><i class="fa fa-dashboard fa-fw"></i> Complainants</a>
                </li>
                <li>
                  <a href="prosecutors.php"><i class="fa fa-dashboard fa-fw"></i> Prosecutors</a>
                </li>
                <li>
                  <a href="procedures.php"><i class="fa fa-dashboard fa-fw"></i> Procedures</a>
                </li>
                <li>
                  <a href="offenses.php"><i class="fa fa-dashboard fa-fw"></i> Offenses</a>
                </li>
                <li>
                  <a href="status.php"><i class="fa fa-dashboard fa-fw"></i> Status</a>
                </li>
                <li>
                  <a href="users.php"><i class="fa fa-dashboard fa-fw"></i> Users</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Tables</h1>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Add Case
        </div><?php include ('scripts/connection.php');?>
        <div class="panel-body">
          <h3 class="text-danger">This is unfinished.</h3>
          <form class="form-horizontal" id="caseAdd" name="caseAdd" action="aa.php" method="POST">
            <div class="form-group">
              <label class="col-sm-2 control-label">NPS Docket No.</label>
              <div class="col-sm-3">
                <input class="form-control" name="npsDocket" type="text">
              </div>
            </div>
            <?php 
              $sql_complainant = "SELECT * FROM tbl_complainants";
              $result_complainant=mysqli_query($conn, $sql_complainant);
              $sql_offenses = "SELECT * FROM tbl_offenses";
              $result_offenses=mysqli_query($conn, $sql_offenses);
              $sql_prosecutor = "SELECT * FROM tbl_prosecutors";
              $result_prosecutor=mysqli_query($conn, $sql_prosecutor);
            ?>
                                  <div class="form-group">
                        <label class="col-md-2 control-label">Complainants Alternative</label>
                        <div class="col-md-3">
                          <select multiple data-plugin-selectTwo class="form-control selecta" name="complainants[]">
                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                <?php while ($row_complainant=mysqli_fetch_array($result_complainant)) { ?>
                  <option>
                    <?php echo $row_complainant['lastname'].", ".$row_complainant['firstname']; ?>
                  </option><?php } ?>
                            <!-- </optgroup> -->
                          </select>
                        </div>
                      </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Respondent</label>
              <div class="col-sm-3">
                <input class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Offense</label>
              <div class="col-sm-3">
                <select class="form-control">
                  <option disabled selected>
                    -Select Offense-
                  </option><?php while ($row_offenses=mysqli_fetch_array($result_offenses)) { ?>
                  <option value="<?php echo $row_offenses['id']; ?>">
                    <?php echo $row_offenses['offense']; ?>
                  </option><?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Prosecutor</label>
              <div class="col-sm-3">
                <select class="form-control">
                  <option disabled selected>
                    -Select Prosecutor-
                  </option>
                  <?php while ($row_prosecutor=mysqli_fetch_array($result_prosecutor)) { ?>
                  <option value="<?php echo $row_prosecutor['id_prosecutor']; ?>">
                    <?php echo $row_prosecutor['lastname'].", ".$row_prosecutor['firstname']; ?>
                  </option><?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Action Taken</label>
              <div class="col-sm-3">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tr>
                    <td>2</td>
                    <td>2</td>
                  </tr>
                  <tr>
                  </tr>

                                
                </table>
              </div>
              <!-- <div class="form-group"> -->
              <!-- <label class="col-sm-2 control-label">Action</label> -->
              <div class="col-sm-3">
                <select class="form-control">
  <option selected>1dasdasda</option>
  <option>2dasdas</option>
  <option>3dasdas</option>
  <option>4das</option>
  <option>5dasdasd</option>
</select>
              </div>
            <!-- </div> -->
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Resolution</label>
              <div class="col-sm-3">
                <input class="form-control" type="file" name="resolution">
              </div>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div><!-- Panel lg-4 end -->
  </div>
  <script src="vendor/jquery/jquery.min.js">
  </script> 
  <script src="vendor/bootstrap/js/bootstrap.min.js">
  </script> 
  <script src="vendor/metisMenu/metisMenu.min.js">
  </script> 
  <script src="vendor/dataTables/DataTables-1.10.16/js/jquery.dataTables.js">
  </script> 
  <script src="vendor/dataTables/DataTables-1.10.16/js/dataTables.bootstrap.js">
  </script> 
  <script src="js/sidemenu.js">
  </script> 
  <script src="vendor/formValidation/formValidation.js">
  </script> 
  <script src="vendor/formValidation/bootstrap_formValidation.js">
  </script> 
  <script src="vendor/bootstrap-multiselect/bootstrap-multiselect.js">
  </script> 
    <script src="vendor/select2/select2.js"></script>

  <script>
  $(document).ready(function() {

    $('#caseAdd')
        .find('[name="complainants[]"]')
            .select2()
            // Revalidate the color when it is changed
            .change(function(e) {
                $('#caseAdd').formValidation('revalidateField', 'complainants[]');
            })
            .end().formValidation({
      excluded: ':disabled',
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        npsDocket: {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        },
        'complainants[]': {
          validators: {
            notEmpty: {
              message: 'This is a required field.'
            }
          }
        },
        resolution: {
        validators: {
            file: {
                extension: 'pdf',
                type: 'application/pdf',
                maxSize: 10*1024*1024,
                message: 'Please choose a pdf file with a size less than 10M.'
            }
        }
    }
      }
    });
    $('select.selecta').select2({
  placeholder: 'Select an option'
});
    $('#example').DataTable();
  });
  </script>
</body>
</html>