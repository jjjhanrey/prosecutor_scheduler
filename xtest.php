<?php 
include_once('core/init.php');
$setting= new Prosecutor();

if (!empty($_POST)) {
	$setting->create(array(
		'access' => Input::get('access'),
		'lastname' => Input::get('lastname'),
		'firstname' => Input::get('firstname'),
		'middlename' => Input::get('middlename'),
		'gender' => Input::get('gender')
	));
	echo "OK";
}
?>